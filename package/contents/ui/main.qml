// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import QtQuick 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.private.mousetoggler 0.1

Item {
    Plasmoid.preferredRepresentation: Plasmoid.compactRepresentation

    Plasmoid.compactRepresentation: PlasmaCore.IconItem {
        readonly property string leftHanded: "input-mouse-click-right"
        readonly property string rightHanded: "input-mouse-click-left"

        function getIcon()
        {
            return MouseTogglerEngine.isLeftHanded() ? leftHanded : rightHanded;
        }

        source: getIcon()

        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onClicked: MouseTogglerEngine.toggleMouse()
        }

        Connections {
            target: MouseTogglerEngine
            function onLeftHandedChanged()
            {
                source = getIcon()
            }
        }
    }
}
