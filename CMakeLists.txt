# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: BSD-2-Clause

cmake_minimum_required(VERSION 3.25.0)
project(mousetoggler LANGUAGES CXX VERSION 0.1.0)

set(QT_MIN_VERSION "5.15")
set(KF5_MIN_VERSION "5.99")

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic")

# Find ECM
find_package(ECM ${KF5_MIN_VERSION} REQUIRED NO_MODULE)
list(APPEND CMAKE_MODULE_PATH ${ECM_MODULE_PATH})
set(KDE_COMPILERSETTINGS_LEVEL "5.99")
include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)

# Find Qt
find_package(Qt5 ${QT_MIN_VERSION} REQUIRED COMPONENTS
    Qml
)
set(CMAKE_AUTOMOC ON)
add_definitions(
    -DQT_NO_CAST_FROM_ASCII
    -DQT_NO_CAST_TO_ASCII
    -DQT_NO_URL_CAST_FROM_STRING
    -DQT_NO_CAST_FROM_BYTEARRAY
    -DQT_DEPRECATED_WARNINGS
    -DQT_STRICT_ITERATORS
    -DQT_DISABLE_DEPRECATED_BEFORE=0x050F00
)

# Find KDE
find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS
    Plasma
    ConfigWidgets
)

# Plugin sources
set(plugin_ROOT ${CMAKE_SOURCE_DIR}/plugin)
set(plugin_SOURCES
    ${plugin_ROOT}/Engine.cpp
    ${plugin_ROOT}/Plugin.cpp
)

# Build the plugin
add_library(mousetogglerplugin SHARED ${plugin_SOURCES})
target_link_libraries(mousetogglerplugin
    PRIVATE
    Qt5::Qml
    KF5::Plasma
    KF5::ConfigWidgets
)

# Install the plasmoid
plasma_install_package(package org.kde.plasma.mousetoggler)

# Install the plugin
install(TARGETS mousetogglerplugin
        DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/plasma/private/mousetoggler)
install(FILES ${plugin_ROOT}/qmldir
        DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/plasma/private/mousetoggler)
