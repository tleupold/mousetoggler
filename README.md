# Mousetoggler

This is a minimalistic plasmoid to easily and quickly switch between right- and left-handed mouse mode.

It displays the current "Input Devices" → "Mouse" → "Left handed mode" setting, and toggles it when it's clicked (either with the left or the right mouse button).

This was inspired by the (now apparently unmaintained) [Mouseswap](https://www.pling.com/p/998954/) plasmoid and the way it presumably worked back then.
