// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "Plugin.h"
#include "Engine.h"

// Qt includes
#include <QtQml>

static QObject *singletonTypeProvider(QQmlEngine *, QJSEngine *)
{
    return new Engine;
}

void Plugin::registerTypes(const char *uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("org.kde.plasma.private.mousetoggler"));
    qmlRegisterSingletonType<Engine>(uri, 0, 1, "MouseTogglerEngine", singletonTypeProvider);
}
