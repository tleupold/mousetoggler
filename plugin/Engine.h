// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef ENGINE_H
#define ENGINE_H

// Qt includes
#include <QObject>

// Qt classes
class QFileSystemWatcher;

class Engine : public QObject
{
    Q_OBJECT

public:
    explicit Engine(QObject *parent = nullptr);
    Q_INVOKABLE bool isLeftHanded() const;
    Q_INVOKABLE void toggleMouse();

Q_SIGNALS:
    void leftHandedChanged();

private Q_SLOTS:
    void rcFileChanged();

private: // Functions
    void getIsLeftHanded();

private: // Variables
    QFileSystemWatcher *m_watcher;
    bool m_isLeftHanded = false;
    QString m_rcFilePath;

};

#endif // MAINWINDOW_H
