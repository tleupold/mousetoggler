// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "Engine.h"

// KDE includes
#include <KConfig>
#include <KConfigGroup>
#include <KPluginFactory>
#include <KCModule>

// Qt includes
#include <QDebug>
#include <QStandardPaths>
#include <QFileSystemWatcher>

// RC file constants
static const QString s_rcFile = QStringLiteral("kcminputrc");
static const QString s_group  = QStringLiteral("Mouse");
static const QString s_entry  = QStringLiteral("XLbInptLeftHanded");

// KCM constants
static const QString s_systemSettingsDirectory = QStringLiteral("plasma/kcms/systemsettings/");
static const QString s_mouseModule = QStringLiteral("kcm_mouse");

Engine::Engine(QObject *parent) : QObject(parent)
{
    m_rcFilePath = QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation)
                   + QLatin1Char('/') + s_rcFile;

    m_watcher = new QFileSystemWatcher({ m_rcFilePath }, this);
    connect(m_watcher, &QFileSystemWatcher::fileChanged, this, &Engine::rcFileChanged);

    getIsLeftHanded();
}

void Engine::getIsLeftHanded()
{
    KConfig config(s_rcFile);
    auto group = config.group(s_group);
    m_isLeftHanded = group.readEntry(s_entry, false);
    Q_EMIT leftHandedChanged();
    qDebug() << "Setting for left-handed changed to" << m_isLeftHanded;
}

bool Engine::isLeftHanded() const
{
    return m_isLeftHanded;
}

void Engine::toggleMouse()
{
    // Set the toggled value
    qDebug() << "Toggling the left-handed setting";
    KConfig config(s_rcFile);
    auto group = config.group(s_group);
    group.writeEntry(s_entry, ! m_isLeftHanded);
    config.sync();

    // Then emit the change by instantiating the plugin, loading and saving it

    qDebug() << "Propagating the change";

    const auto metaData = KPluginMetaData::findPluginById(s_systemSettingsDirectory, s_mouseModule);
    if (! metaData.isValid()) {
        qWarning() << "Could not load the" << s_mouseModule << "module's metadata!";
        return;
    }

    auto result = KPluginFactory::loadFactory(metaData);
    if (! result) {
        qWarning() << "Could not load the" << s_mouseModule << "factory!";
        return;
    }

    auto *kcm = result.plugin->create<KCModule>(this);
    kcm->load();
    kcm->save();
    kcm->deleteLater();
}

void Engine::rcFileChanged()
{
    // When the rc file is changed, a new one is created and moved to the original location.
    // We thus have to re-add it here, as it's another file after the change.
    if (! m_watcher->files().contains(m_rcFilePath)) {
        m_watcher->addPath(m_rcFilePath);
    }

    getIsLeftHanded();
}
